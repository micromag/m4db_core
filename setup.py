#!/usr/scripts/env python

from distutils.core import setup

version = "1.0.0"

setup(name="m4db_core",
      version=version,
      license="MIT",
      description="M4DB core libraries",
      url="https://bitbucket.org/micromag/m4db_core/src/master/",
      download_url="https://bitbucket.org/micromag/m4db_core/get/m4db_core-{}.zip".format(version),
      keywords=["micromagnetics"],
      install_requires=["numpy",
                        "requests",
                        "Jinja2",
                        "sklearn",
                        "scikit-learn",
                        "matplotlib",
                        "urllib3",
                        "vtk"],
      author="L. Nagy, W. Williams",
      author_email="lnagy2@ed.ac.uk",
      packages=["m4db_core", "m4db_thirdparty"],
      package_dir={
          "m4db_core": "lib/m4db_core",
          "m4db_thirdparty": "lib/m4db_thirdparty"
      },
      package_data={
          "m4db_core": ["templates/merrill/*.jinja2", "templates/slurm/*.jinja2"]
      },
      classifiers=[
            "Development Status :: 3 - Alpha",
            "Intended Audience :: Developers",
            "Topic :: Database",
            "License :: OSI Approved :: MIT License",
            "Programming Language :: Python :: 3",
            "Programming Language :: Python :: 3.6",
            "Programming Language :: Python :: 3.7",
            "Programming Language :: Python :: 3.8",
      ])
