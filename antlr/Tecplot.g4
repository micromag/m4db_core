/** This is a grammar to recognise Tecplot files */
grammar Tecplot;

start : title variables zone
      ;

title : KW_TITLE ASSIGN STRING_LITERAL
      ;

variables : KW_VARIABLES ASSIGN VARIABLE (',' VARIABLE)*
          ;

zone : KW_ZONE zone_title ','? zone_n ','? zone_e ','? zone_f ','? zone_et ','? zone_varlocation?
     ;

zone_title : SYMB_ZONE_T ASSIGN STRING_LITERAL
           ;

zone_n : SYMB_ZONE_N ASSIGN INT
       ;

zone_e : SYMB_ZONE_E ASSIGN INT
       ;

zone_f : SYMB_ZONE_F ASSIGN zone_f_type
       ;

zone_f_type : SYMB_F_FEPOINT
            ;

zone_et : SYMB_ZONE_ET ASSIGN zone_et_type
        ;

zone_et_type : SYMB_ET_TETRAHEDRON
             ;

float_list : FLOAT+;

/** Lexer tokens */
KW_TITLE                    : 'TITLE';
KW_VARIABLES                : 'VARIABLES';
KW_ZONE                     : 'ZONE';

SYMB_ZONE_T                 : 'T';
SYMB_ZONE_N                 : 'N';
SYMB_ZONE_E                 : 'E';
SYMB_ZONE_F                 : 'F';
SYMB_ZONE_ET                : 'ET';
SYMB_ZONE_VARLOCATION       : 'VARLOCATION';

SYMB_F_FEPOINT              : 'FEPOINT';
SYMB_ET_TETRAHEDRON         : 'TETRAHEDRON';

VARIABLE                    : '"' [A-Za-z]+ '"';
STRING_LITERAL              : UNTERMINATED_STRING_LITERAL '"';
UNTERMINATED_STRING_LITERAL : '"' (~["\\\r\n] | '\\' (. | EOF))*;
ASSIGN                      : '=';

WS                          : [ \n\t\r]+ -> skip;
INT                         : [0-9]+;
FLOAT                       : '-'? [0-9]+ '.' [0-9]+ 'E' ('+' | '-') [0-9]+;

