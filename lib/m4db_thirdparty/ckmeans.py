r"""
An implementation of the ckmeans clustering algorithm an optimal 1d clustering
algorithm by Wang & Song, 2011.

The original was ported to Python by
Sam Hocevar and is available at https://github.com/llimllib/ckmeans.git

This version contains a small addition by L. Nagy so that it works
on arrays of objects and computes the optimal number of clusters based on
the silhouette method.

Wang & Song,
Ckmeans.1d.dp: optimal k-means clustering in one dimension by dynamic programming
Haizhou Wang & Mingzhou Song
The R Journal v3, no. 2, pg. 29
2011
"""
import numpy as np

from sklearn.metrics import silhouette_score


def ssq(j, i, sum_x, sum_x_sq):
    if j > 0:
        muji = (sum_x[i] - sum_x[j - 1]) / (i - j + 1)
        sji = sum_x_sq[i] - sum_x_sq[j - 1] - (i - j + 1) * muji ** 2
    else:
        sji = sum_x_sq[i] - sum_x[i] ** 2 / (i + 1)

    return 0 if sji < 0 else sji


def fill_row_k(imin, imax, k, s_matrix, j_matrix, sum_x, sum_x_sq, n):
    if imin > imax:
        return

    i = (imin + imax) // 2
    s_matrix[k][i] = s_matrix[k - 1][i - 1]
    j_matrix[k][i] = i

    jlow = k

    if imin > k:
        jlow = int(max(jlow, j_matrix[k][imin - 1]))
    jlow = int(max(jlow, j_matrix[k - 1][i]))

    jhigh = i - 1
    if imax < n - 1:
        jhigh = int(min(jhigh, j_matrix[k][imax + 1]))

    for j in range(jhigh, jlow - 1, -1):
        sji = ssq(j, i, sum_x, sum_x_sq)

        if sji + s_matrix[k - 1][jlow - 1] >= s_matrix[k][i]:
            break

        # Examine the lower bound of the cluster border
        # compute s(jlow, i)
        sjlowi = ssq(jlow, i, sum_x, sum_x_sq)

        ssq_jlow = sjlowi + s_matrix[k - 1][jlow - 1]

        if ssq_jlow < s_matrix[k][i]:
            s_matrix[k][i] = ssq_jlow
            j_matrix[k][i] = jlow

        jlow += 1

        ssq_j = sji + s_matrix[k - 1][j - 1]
        if ssq_j < s_matrix[k][i]:
            s_matrix[k][i] = ssq_j
            j_matrix[k][i] = j

    fill_row_k(imin, i - 1, k, s_matrix, j_matrix, sum_x, sum_x_sq, n)
    fill_row_k(i + 1, imax, k, s_matrix, j_matrix, sum_x, sum_x_sq, n)


def fill_dp_matrix(data, s_matrix, j_matrix, nk, n, key=lambda x: x):
    sum_x = np.zeros(n, dtype=np.float_)
    sum_x_sq = np.zeros(n, dtype=np.float_)

    # median. used to shift the values of x to improve numerical stability
    shift = key(data[n // 2])

    for i in range(n):
        if i == 0:
            sum_x[0] = key(data[0]) - shift
            sum_x_sq[0] = (key(data[0]) - shift) ** 2
        else:
            sum_x[i] = sum_x[i - 1] + key(data[i]) - shift
            sum_x_sq[i] = sum_x_sq[i - 1] + (key(data[i]) - shift) ** 2

        s_matrix[0][i] = ssq(0, i, sum_x, sum_x_sq)
        j_matrix[0][i] = 0

    for k in range(1, nk):
        if k < nk - 1:
            imin = max(1, k)
        else:
            imin = n - 1

        fill_row_k(imin, n - 1, k, s_matrix, j_matrix, sum_x, sum_x_sq, n)


def ckmeans(data, n_clusters, key=lambda x: x):
    if n_clusters <= 0:
        raise ValueError("Cannot classify into 0 or less clusters")
    if n_clusters > len(data):
        raise ValueError("Cannot generate more classes than there are data values")

    data.sort(key=key)
    n = len(data)

    s_matrix = np.zeros((n_clusters, n), dtype=np.float_)

    j_matrix = np.zeros((n_clusters, n), dtype=np.uint64)

    fill_dp_matrix(data, s_matrix, j_matrix, n_clusters, n, key=key)

    clusters = []
    cluster_right = n - 1

    for cluster in range(n_clusters - 1, -1, -1):
        cluster_left = int(j_matrix[cluster][cluster_right])
        clusters.append(data[cluster_left:cluster_right + 1])

        if cluster > 0:
            cluster_right = cluster_left - 1

    return list(reversed(clusters))


def cluster_and_silhouette(data, n_clusters, key=lambda x: x):
    r"""
    Compute an n clustering on the input data array.
    :param data: the data array of values to be clustered
    :param n_clusters: the the number of clusters to use
    :param key: a function that may be used to select which value to cluster on - this is useful if an array of
                objects has been specified instead of an array of numbers.
    :return: a pair with the first value being the clustered input data and the second value the silhouette of the
             clustering.
    """
    clusters = ckmeans(data, n_clusters, key)

    # Extract an array of values
    data_points = []
    data_labels = []
    cluster_label = 0
    for cluster in clusters:

        for obj in cluster:
            data_points.append([key(obj)])
            data_labels.append(cluster_label)
        cluster_label += 1

    # Compute silhouette
    silhouette = silhouette_score(data_points, data_labels)

    return clusters, silhouette


def optimal_cluster(data, n_max, key=lambda x: x):
    r"""
    Attempts to find the optimal clustering for the input data using the silhouette method.
    :param data: the data array of values to be clustered
    :param n_max: the maximum number of clusters to search through
    :param key: a function that may be used to select which value to cluster on - this is useful if an array of
                objects has been specified instead of an array of numbers.
    :return: the 'optimally clustered' objects
    """

    cluster_list = []
    # TODO: check this logic!
    true_max = min(len(data), n_max)

    for n in range(2, true_max + 1):
        try:
            cluster_list.append(
                cluster_and_silhouette(data, n, key)
            )
        except ValueError:
            # This cluster attempt resulted in a an error, so just break the loop
            break

    best_cluster = max(cluster_list, key=lambda p: p[1])
    return best_cluster[0]
