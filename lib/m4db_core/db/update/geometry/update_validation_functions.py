r"""
A set of utility functions for validation of JSON data that corresponds with updates.
"""
import traceback
import json


def json_validate_add_geometry(str_json):
    r"""
    Validates a JSON string.
    Args:
        str_json: the JSON string to validate.

    Returns: a pair with the first parameter a boolean indicating success and the second parameter a string
             describing the reason for failure; the boolean part is True if the data entered is valid, and the
             string part is only populated if the boolean part is False.

    """
    try:
        pydict = json.loads(str_json)
    except:
        return False, traceback.print_exc()

    return pydict_validate_add_geometry(pydict)


def pydict_validate_add_geometry(data):
    r"""

    Args:
        data: python dictionary to validate.

    Returns: a pair with the first parameter a boolean indicating success and the second parameter a string
             describing the reason for failure; the boolean part is True if the data entered is valid, and the
             string part is only populated if the boolean part is False.

    """

    # Check that all of our required fields are present.
    if "name" not in data.keys() or data["name"] is None:
        return False, "key 'name' was not in the request"
    if "size" not in data.keys() or data["size"] is None:
        return False, "key 'size' was not in request"
    if "size_unit" not in data.keys() or data["size_unit"] is None:
        return False, "key 'size_unit' was not in request"
    if "size_convention" not in data.keys() or data["size_convention"] is None:
        return False, "key 'size_convention' was not in request"

    # Flags to indicate the presence of patran or exodus data files.
    if data.get("patran_data") is None:
        has_patran_data = False
    else:
        has_patran_data = True

    if data.get("exodus_data") is None:
        has_exodus_data = False
    else:
        has_exodus_data = True

    # Verify that we have either patran data or exodus data
    if has_patran_data == False and has_exodus_data == False:
        return False, "Must have either patran data or exodus data, but found neither in request"
