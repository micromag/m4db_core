r"""
A set of functions to update the database.
"""
import os

from m4db_database.orm import Geometry, Unit, SizeConvention, Software


def add_geometry(session, name, size, size_unit, size_convention, nelements, nvertices, nsubmeshes,
                 element_size=None, element_size_unit=None, description=None, volume_total=None, software_name=None,
                 software_version=None, patran_data=None, exodus_data=None, mesh_gen_script_data=None,
                 mesh_gen_output_data=none):
    r"""

    Args:
        session: the database session
        name: the model name
        size: the size of the geometry
        size_unit: the unit of the geometry size
        size_convention: the size convetion being used "ESVD" or "ECVL"
        nelements: the number of elements in the geometry mesh
        nvertices: the number of vertices in the geometry mesh
        nsubmeshes: the number of geometry submeshes
        element_size: the element size used to generate the mesh (optional)
        element_size_unit:  the unit of the element size (optional)
        description: the geometry description (optional)
        volume_total: the volume of the mesh (optional)
        software_name: the software used to generate the mesh (optional)
        software_version: the software version used to generate the mesh (optional)
        mesh_gen_script_data: the mesh generation script file (optional)
        mesh_gen_output_data: the mesh output data file (optional)
        patran_data: the patran mesh data file (either this or exodus_data must be supplied)
        exodus_data: the exodus mesh data file (either this of patran_data must be supplied)

    Returns: a pair with the first parameter a boolean indicating success and the second parameter a string
             describing the reason for failure; the boolean part is True if the data entered is valid, and the
             string part is only populated if the boolean part is False.
    """
    if patran_data is None and exodus_data is None:
        return False, "Either patran_data or exodus_data must be supplied."

    if mesh_gen_script_data:
        if not os.path.isfile(mesh_gen_script_data):
            return False, "Mesh generation script file not found: '{}'".format(mesh_gen_script_data)
        has_mesh_gen_script_data = True
    else:
        has_mesh_gen_script_data = False

    if mesh_gen_output_data:
        if not os.path.isfile(mesh_gen_output_data):
            return False, "Mesh generation output file not found: '{}'".format(mesh_gen_output_data)
        has_mesh_gen_output_data = True
    else:
        has_mesh_gen_output_data = False

    if patran_data:
        if not os.path.isfile(patran_data):
            return False, "Patran file not found: '{}'".format(patran_data)
        has_patran_data = True
    else:
        has_patran_data = False

    if exodus_data:
        if not os.path.isfile(exodus_data):
            return False, "Exodus file not found: '{}'".format(exodus_data)
        has_exodus_data = True
    else:
        has_exodus_data = False

    ##
    # Query the parts of the new geometry that are requred.
    ##

    # Retrieve the size unit.
    db_size_unit = session.query(Unit).filter(Unit.symbol == size_unit).one_or_none()
    if size_unit is None:
        return False, "size unit with symbol '{}' could not be found".format(size_unit)

    # Retrieve the size convention.
    db_size_convention = session.query(SizeConvention).filter(SizeConvention.symbol == size_convention).one_or_none()
    if size_convention is None:
        return False, "size convention with symbol '{}' could not be found".format(data["size_convention"])

    ##
    # Query the parts of the new geometry that are optional.
    ##

    # Retrieve the element size unit
    if element_size_unit is not None:
        db_element_size_unit = session.query(Unit).filter(Unit.symbol == element_size_unit).one_or_none()
        if element_size_unit is None:
            return False, "element size unit was given, but symbol {} not found".format(element_size_unit)
    else:
        db_element_size_unit = None

    if software_name is not None and software_version is not None:
        db_software = session.query(Software). \
            filter(Software.name == software_name). \
            filter(Software.version == software_version). \
            one_or_none()
        if db_software is None:
            return False, "software was given but {} ({}) was not found".format(software_name, software_version)
    else:
        db_software = None

    # The new geometry
    geometry = Geometry(
        name=name,
        size=size,
        size_unit=db_size_unit,
        nelements=nelements,
        nvertices=nvertices,
        nsubmeshes=nsubmeshes,
        element_size=element_size,
        element_size_unit=db_element_size_unit,
        description=description,
        volume_total=volume_total,
        size_convention=db_size_convention,
        software=db_software,
        has_patran=has_patran_data,
        has_exodus=has_exodus_data,
        has_mesh_gen_script=has_mesh_gen_script_data,
        has_mesh_gen_output=has_mesh_gen_output_data
    )

    # Add & commit the session (should now have a unique id)
    session.add(geometry)
    session.commit()

    return True, ""
