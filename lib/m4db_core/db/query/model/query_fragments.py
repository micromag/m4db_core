r"""
A set of utility functions that are useful for building complex queries.
"""

from sqlalchemy import tuple_

from m4db_database.orm import RunningStatus
from m4db_database.orm import Model
from m4db_database.orm import Geometry
from m4db_database.orm import SizeConvention
from m4db_database.orm import Metadata
from m4db_database.orm import Project
from m4db_database.orm import DBUser
from m4db_database.orm import Material
from m4db_database.orm import ModelMaterialAssociation

from m4db_core.unit_conversion import convert_to_micron


def _preprocess_model_size(size, size_unit=None):
    """
    Convert a size with size_unit unless it has the specific value 'all' in which case the test string 'all' is
    returned.
    :param size: the size to convert or the keyword 'all'
    :param size_unit: the size unit, if size_unit is not 'all' then this must be set
    :return: size with size_unit converted to micron or the text string 'all'
    """
    if size != "all":
        if size_unit is None:
            raise ValueError("Parameter 'size' is not 'all' but 'size_unit' is missing")
        size_in_micron = convert_to_micron(float(size), size_unit)
    else:
        size_in_micron = "all"

    return size_in_micron


def _preprocess_model_size_convention(size_convention):
    r"""
    Convert the input size_convention to all upper case, unless it is the special value 'all', in which case return the
    word 'all'.
    :param size_convention: the size convention
    :return: the value of size_convention as all upper case or the keyword 'all'
    """
    if size_convention != "all":
        size_convention_upper = size_convention.upper()
    else:
        size_convention_upper = "all"

    return size_convention_upper


def _get_models_by_mgst_query_body(select_clause_base,
                                   session,
                                   run_status,
                                   user_name,
                                   project_name,
                                   materials,
                                   geometry,
                                   size_in_micron,
                                   size_convention_upper,
                                   temperatures):

    r"""
    Construct a query fragment that will include a search by materials, geometry, size and temperature.
    :param select_clause_base: the select part of the query to pick out columns required by the query.
    :param session: the session which communicates with the database.
    :param run_status: the run status that we require or the special keyword 'all'.
    :param user_name: the user name that we require or the special keyword 'all'.
    :param project_name: the project name that we require or the special keyword 'all'.
    :param materials: a list of material names that we require.
    :param geometry: the geometry that we require or the special keyword 'all'.
    :param size_in_micron: the size *IN MICRON*.
    :param size_convention_upper: the size convention *IN UPPER CASE*.
    :param temperatures: a list of temperatures.
    :return: a query that will append the 'FROM' clause to select_base_clause.
    """

    models_query = select_clause_base.join(RunningStatus, Model.running_status_id == RunningStatus.id). \
                                      join(Geometry, Model.geometry_id == Geometry.id). \
                                      join(SizeConvention, Geometry.size_convention_id == SizeConvention.id). \
                                      join(Metadata, Model.mdata_id == Metadata.id). \
                                      join(Project, Metadata.project_id == Project.id). \
                                      join(DBUser, Metadata.db_user_id == DBUser.id)

    # If the running status is 'all' do not apply filter on this field
    if run_status != "all":
        models_query = models_query.filter(RunningStatus.name == run_status)

    # if the user name is 'all' do not apply filter on this field
    if user_name != "all":
        models_query = models_query.filter(DBUser.user_name == user_name)

    # If the project name is 'all' do not apply filter on this field
    if project_name != "all":
        models_query = models_query.filter(Project.name == project_name)

    # If the geometry is 'all' then do not apply filter on this field
    if geometry != "all":
        models_query = models_query.filter(Geometry.name == geometry)

    # If the geomery size is 'all' then do not filter on this field
    if size_in_micron != "all":
        models_query = models_query.filter(Geometry.size == size_in_micron)

    # If the size convention is all then do not filter on this field
    if size_convention_upper != "all":
        models_query = models_query.filter(SizeConvention.symbol == size_convention_upper)

    # Here we deal with the materials and temperatures
    for material, temperature in zip(materials, temperatures):
        subquery = session.query(Material.name, Material.temperature). \
            join(ModelMaterialAssociation, Material.id == ModelMaterialAssociation.material_id). \
            filter(ModelMaterialAssociation.model_id == Model.id)

        models_query = models_query.filter(tuple_(material, temperature).in_(subquery))

    return models_query


def get_models_quants_by_mgst_query(session,
                                    run_status,
                                    user_name,
                                    project_name,
                                    materials,
                                    geometry,
                                    size,
                                    size_convention,
                                    temperatures,
                                    size_unit):
    r"""
    Build a query that will retrieve model quants by material, geometry, size and temperature.
    :param session: the session which communicates with the database.
    :param run_status: the run status that we require or the special keyword 'all'.
    :param user_name: the user_name that we require or the special keyword 'all'.
    :param project_name: the project name that we require or the special keyword 'all'.
    :param materials: the list of materials.
    :param geometry: the geometry that we require or the special keyword 'all'.
    :param size: the size that we require or the special keyword 'all'
    :param size_convention: the size convention that we require or the special keyword 'all'.
    :param temperatures: the list of temperatures.
    :param size_unit: if size_temperature equal to 'all' then this is ignored, otherwise the size unit corresponding to
                      size_temperature
    :return: a query that will return model quants when we search by material, geometry, size, temperature, and admin
             data.
    """

    size_in_micron = _preprocess_model_size(size, size_unit)
    size_convention_upper = _preprocess_model_size_convention(size_convention)

    select_clause_base = session.query(
        Model.unique_id,
        Model.mx_tot,
        Model.my_tot,
        Model.mz_tot,
        Model.vx_tot,
        Model.vy_tot,
        Model.vz_tot,
        Model.h_tot,
        Model.adm_tot,
        Model.e_typical,
        Model.e_anis,
        Model.e_ext,
        Model.e_demag,
        Model.e_exch1,
        Model.e_exch2,
        Model.e_exch3,
        Model.e_exch4,
        Model.e_tot
    )

    models_query = _get_models_by_mgst_query_body(
        select_clause_base,
        session,
        run_status,
        user_name,
        project_name,
        materials,
        geometry,
        size_in_micron,
        size_convention_upper,
        temperatures
    )

    return models_query

def get_models_by_mgst_query(session,
                             run_status,
                             user_name,
                             project_name,
                             materials,
                             geometry,
                             size,
                             size_unit,
                             size_convention,
                             temperatures):
    r"""
    Build a query that will retrieve model objects by material, geometry, size and temperature; *WARNING* this is an
    expensive query to run!
    :param session: the session which communicates with the database.
    :param run_status: the run status that we require or the special keyword 'all'.
    :param user_name: the user_name that we require or the special keyword 'all'.
    :param project_name: the project name that we require or the special keyword 'all'.
    :param materials: the list of materials.
    :param geometry: the geometry that we require or the special keyword 'all'.
    :param size: the size that we require or the special keyword 'all'
    :param size_convention: the size convention that we require or the special keyword 'all'.
    :param temperatures: the list of temperatures.
    :param size_unit: if size_temperature equal to 'all' then this is ignored, otherwise the size unit corresponding to
                      size_temperature
    :return: a query that will return model objects when we search by material, geometry, size, temperature, and admin
             data.
    """
    size_in_micron = _preprocess_model_size(size, size_unit)
    size_convention_upper = _preprocess_model_size_convention(size_convention)

    select_clause_base = session.query(Model)

    models_query = _get_models_by_mgst_query_body(
        select_clause_base,
        session,
        run_status,
        user_name,
        project_name,
        materials,
        geometry,
        size_in_micron,
        size_convention_upper,
        temperatures
    )

    return models_query


def get_model_unique_ids_by_mgst_query(session,
                                       run_status,
                                       user_name,
                                       project_name,
                                       materials,
                                       geometry,
                                       size,
                                       size_unit,
                                       size_convention,
                                       temperatures):
    r"""
    Build a query that will retrieve model unique ids by material, geometry, size and temperature.
    :param session: the session which communicates with the database.
    :param run_status: the run status that we require or the special keyword 'all'.
    :param user_name: the user_name that we require or the special keyword 'all'.
    :param project_name: the project name that we require or the special keyword 'all'.
    :param materials: the list of materials.
    :param geometry: the geometry that we require or the special keyword 'all'.
    :param size: the size that we require or the special keyword 'all'
    :param size_convention: the size convention that we require or the special keyword 'all'.
    :param temperatures: the list of temperatures.
    :param size_unit: if size_temperature equal to 'all' then this is ignored, otherwise the size unit corresponding to
                      size_temperature
    :return: a query that will return model unique ids when we search by material, geometry, size, temperature, and
             admin data.
    """

    size_in_micron = _preprocess_model_size(size, size_unit)
    size_convention_upper = _preprocess_model_size_convention(size_convention)

    select_clause_base = session.query(Model.unique_id)

    models_unique_id_query = _get_models_by_mgst_query_body(
        select_clause_base,
        session,
        run_status,
        user_name,
        project_name,
        materials,
        geometry,
        size_in_micron,
        size_convention_upper,
        temperatures
    )

    return models_unique_id_query
