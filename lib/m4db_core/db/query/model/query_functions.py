r"""
A set of functions to query the database revolving around models. These methods will *NOT* change the database.
"""

from m4db_database.orm import Model

from m4db_core.db.query.model.query_fragments import get_models_quants_by_mgst_query
from m4db_core.db.query.model.query_fragments import get_models_by_mgst_query
from m4db_core.db.query.model.query_fragments import get_model_unique_ids_by_mgst_query

def model_unique_id_exists(session, unique_id):
    r"""
    Test to see whether the model unique id exists.

    :param session: the session that communicates with the database.
    :param unique_id: a unique id for a model.
    :return: True if a model with unique id exists in the database otherwise False.
    """
    unique_id = session.query(Model.unique_id).\
        filter(Model.unique_id == unique_id).\
        one_or_none()

    if unique_id is not None:
        return False
    return True


def model_by_unique_id(session, unique_id):
    r"""
    Retrieve a Model database object by unique id.
    :param session: the session that communicates with the database.
    :param unique_id: a model unique id
    :return: a model from the database (or None)
    """
    model = session.query(Model).\
        filter(Model.unique_id == unique_id).\
        one_or_none()

    return model.as_dict()


def model_quants_by_mgst(session, run_status="all", user_name="all", project_name="all", materials=[],
                         geometry="all", size="all", size_convention="all", temperatures=[], size_unit="all"):
    r"""
    Retrieve a set of Model quant data by material, geometry, size, temperature (additionally some other admin
    information).
    :param session: the session that communicates with the database.
    :param materials: a list of material names that the model(s) must have.
    :param temperatures: a list of temperatures at which a model(s) was completed.
    :param run_status: the running status of a model(s) (by default 'all' models).
    :param user_name: the user name associated with the model(s) (by default 'all' users).
    :param project_name: the project name associated with the model(s) (by default 'all' project names).
    :param geometry: the geometry associated with the model(s).
    :param size: the size associated with the model(s).
    :param size_unit: the size unit associated with the model(s).
    :param size_convention: the size convention associated with the model(s).
    :return: a list of python dictionaries with the following form
            {
                'unique_id': <the unique id of a model>,
                'mx_tot': <the magnetization x direction integrated over the volume>,
                'my_tot': <the magnetization y direction integrated over the volume>,
                'mz_tot': <the magnetization z direction integrated over the volume>,
                'vx_tot': <the vorticity field x direction integrated over the volume>,
                'vy_tot': <the vorticity field y direction integrated over the volume>,
                'vz_tot': <the vorticity field z direction integrated over the volume>,
                'h_tot': <the helicity field integrated over the volume>,
                'adm_tot': <the ADM field integrated over the volume>,
                'e_typical': <the typical energy value>,
                'e_anis': <the anisotropy energy value>,
                'e_ext': <the external field energy value>,
                'e_demag': <the demagnetizing field energy value>,
                'e_exch1': <the exchange energy value>,
                'e_exch2': <the exchange energy value (second variant)>,
                'e_exch3': <the exchange energy value (third variant)>,
                'e_exch4': <the exchange energy value (fourth variant)>,
                'e_tot': <the total energy>,
            }
    """
    quant_query = get_models_quants_by_mgst_query(
        session=session,
        run_status=run_status,
        user_name=user_name,
        project_name=project_name,
        materials=materials,
        geometry=geometry,
        size=size,
        size_convention=size_convention,
        temperatures=temperatures,
        size_unit=size_unit
    )

    quants = quant_query.all()

    quants_out = []
    for (uid, mx_tot, my_tot, mz_tot, vx_tot, vy_tot, vz_tot, h_tot, adm_tot, e_typical, e_anis, e_ext, e_demag,
         e_exch1, e_exch2, e_exch3, e_exch4, e_tot) in quants:
        quants_out.append({
            'unique_id': uid,
            'mx_tot': mx_tot,
            'my_tot': my_tot,
            'mz_tot': mz_tot,
            'vx_tot': vx_tot,
            'vy_tot': vy_tot,
            'vz_tot': vz_tot,
            'h_tot': h_tot,
            'adm_tot': adm_tot,
            'e_typical': e_typical,
            'e_anis': e_anis,
            'e_ext': e_ext,
            'e_demag': e_demag,
            'e_exch1': e_exch1,
            'e_exch2': e_exch2,
            'e_exch3': e_exch3,
            'e_exch4': e_exch4,
            'e_tot': e_tot
        })

    return quants_out


def models_by_mgst(session, run_status="all", user_name="all", project_name="all", materials=[],
                         geometry="all", size="all", size_convention="all", temperatures=[], size_unit="all"):
    r"""
    Retrieve a set of Model data by material, geometry, size, temperature (additionally some other admin
    information).
    :param session: the session that communicates with the database.
    :param materials: a list of material names that the model(s) must have.
    :param temperatures: a list of temperatures at which a model(s) was completed.
    :param run_status: the running status of a model(s) (by default 'all' models).
    :param user_name: the user name associated with the model(s) (by default 'all' users).
    :param project_name: the project name associated with the model(s) (by default 'all' project names).
    :param geometry: the geometry associated with the model(s).
    :param size: the size associated with the model(s).
    :param size_unit: the size unit associated with the model(s).
    :param size_convention: the size convention associated with the model(s).
    :return: a list of python dictionaries containing Model serialized data.
    """
    model_query = get_models_by_mgst_query(
        session=session,
        run_status=run_status,
        user_name=user_name,
        project_name=project_name,
        materials=materials,
        geometry=geometry,
        size=size,
        size_convention=size_convention,
        temperatures=temperatures,
        size_unit=size_unit
    )

    models = model_query.all()

    models_out = [model.as_dict() for model in models]

    return models_out


def models_unique_ids_by_mgst(session, run_status="all", user_name="all", project_name="all", materials=[],
                         geometry="all", size="all", size_convention="all", temperatures=[], size_unit="all"):
    r"""
    Retrieve a set of Model unique ids by material, geometry, size, temperature (additionally some other admin
    information).
    :param session: the session that communicates with the database.
    :param materials: a list of material names that the model(s) must have.
    :param temperatures: a list of temperatures at which a model(s) was completed.
    :param run_status: the running status of a model(s) (by default 'all' models).
    :param user_name: the user name associated with the model(s) (by default 'all' users).
    :param project_name: the project name associated with the model(s) (by default 'all' project names).
    :param geometry: the geometry associated with the model(s).
    :param size: the size associated with the model(s).
    :param size_unit: the size unit associated with the model(s).
    :param size_convention: the size convention associated with the model(s).
    :return: a list of unique ids.
    """
    unique_id_query = get_model_unique_ids_by_mgst_query(
        session=session,
        run_status=run_status,
        user_name=user_name,
        project_name=project_name,
        materials=materials,
        geometry=geometry,
        size=size,
        size_convention=size_convention,
        temperatures=temperatures,
        size_unit=size_unit
    )

    unique_ids_out = [uid[0] for uid in unique_id_query.all()]  # Because SQLAlchemy returns 1-tuples of unique ids.

    return unique_ids_out
