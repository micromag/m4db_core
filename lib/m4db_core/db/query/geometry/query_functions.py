r"""
A set of functions to query the database. These methods will *NOT* change the database.
"""
from sqlalchemy import text

from m4db_database.orm import Geometry


def geometry_unique_id_exists(session, unique_id):
    r"""
    Test to see whether the geometry unique id exists.

    :param session: the session that communicates with the database.
    :param unique_id: a unique id for a geometry.
    :return: True if a geometry with unique_id exists in the database otherwise false.
    """
    unique_id = session.query(Geometry.unique_id).\
        filter(Geometry.unique_id == unique_id) .\
        one_or_none()

    if unique_id is not None:
        return True
    return False


def geometry_by_unique_id(session, unique_id):
    r"""
    Retrieve a Geometry database object by unique id.
    :param session: the session that communicates with the database.
    :param unique_id: a geometry unique id
    :return: a geometry object from the database (or None)
    """
    geometry = session.query(Geometry). \
        filter(Geometry.unique_id == unique_id). \
        one_or_none()

    if geometry is None:
        return None
    else:
        return geometry.as_dict()


def geometry_summary(session):
    r"""
    Retrieve a set of geometry summary data that summarizes all geometries on the system.

    :param session: the session that communicates with the database.
    :return: an array of dictionaries, of the form
        {'name': <the geometry name>,
         'size': <the geometry size>,
         'unit': <the geometry unit>,
         'size_convention' : <the geometry size convention>,
         'unique_id' : <the unique id of the geometry>
         'description' : <the description of the geometry>
        }
    """
    summary = session.execute(
        text(
            """
            select distinct
                geometry.name,
                geometry.size,
                unit.symbol,
                size_convention.symbol,
                geometry.unique_id, 
                geometry.description
            from geometry
            inner join unit
                on geometry.size_unit_id = unit.id
            inner join size_convention
                on geometry.size_convention_id = size_convention.id
            order by
                geometry.name,
                geometry.size
            """
        )
    )

    # construct list of summary data records
    summary_data = []
    for (name, size, unit, size_convention, unique_id, description) in summary:
        summary_data.append({
            'name': name,
            'size': size,
            'unit': unit,
            'size_convention': size_convention,
            'unique_id': unique_id,
            'description': description
        })

    return summary_data
