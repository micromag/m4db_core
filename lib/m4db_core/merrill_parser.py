import re

FLOAT_STR = r'[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?'


def stdout(str_stdout):
    r"""
    This function will parse some merrill standard output and produce a
    python dictionary of values (see output).

    Arguments:
        str_stdout : the stdout string produced by merrill

    Output:
        a python dictionary with the following keys
            version - the merrill version
    """
    regex_joule_mode = re.compile(r'^Energies in units of J:$')
    regex_anis_energy = re.compile(r'^\s*E-Anis\s*({float:})\s*$'.format(
        float=FLOAT_STR
    ))
    regex_ext_energy = re.compile(r'^\s*E-Ext\s*({float:})\s*$'.format(
        float=FLOAT_STR
    ))
    regex_demag_energy = re.compile(r'^\s*E-Demag\s*({float:})\s*$'.format(
        float=FLOAT_STR
    ))
    regex_exch1_energy = re.compile(r'^\s*E-Exch\s*({float:})\s*$'.format(
        float=FLOAT_STR
    ))
    regex_exch2_energy = re.compile(r'^\s*E-Exch2\s*({float:})\s*$'.format(
        float=FLOAT_STR
    ))
    regex_exch3_energy = re.compile(r'^\s*E-Exch3\s*({float:})\s*$'.format(
        float=FLOAT_STR
    ))
    regex_exch4_energy = re.compile(r'^\s*E-Exch4\s*({float:})\s*$'.format(
        float=FLOAT_STR
    ))
    regex_tot_energy = re.compile(r'^\s*E-Tot\s*({float:})\s*$'.format(
        float=FLOAT_STR
    ))

    regex_avg_mag_mode = re.compile(r'^Average magnetization:$')
    regex_mx = re.compile(r'^\s*<Mx>\s*({float:})\s*$'.format(
        float=FLOAT_STR
    ))
    regex_my = re.compile(r'^\s*<My>\s*({float:})\s*$'.format(
        float=FLOAT_STR
    ))
    regex_mz = re.compile(r'^\s*<Mz>\s*({float:})\s*$'.format(
        float=FLOAT_STR
    ))

    regex_typical_energy_mode = re.compile('^Typical energy scale.*$')
    regex_typical_energy_joule = re.compile(r'^\s*Typical Energy \(J\)\s*({float:})\s*$'.format(
        float=FLOAT_STR
    ))
    regex_typical_energy_kvd = re.compile(r'^\s*Typical Energy \(Kd V\)\s*({float:})\s*$'.format(
        float=FLOAT_STR
    ))

    regex_delta_f = re.compile(
        r'^\s*Delta F negligible:\s*({float:})\s*$'.format(
            float=FLOAT_STR
        )
    )

    regex_minimization_finished = re.compile(
        r'^\s*MINIMIZATION FINISHED\s*$'
    )

    regex_failed_to_converge = re.compile(
        r'^.*FAILED TO CONVERGE.*$'
    )

    result = {
        'anis_energy'          : None,
        'ext_energy'           : None,
        'demag_energy'         : None,
        'exch1_energy'         : None,
        'exch2_energy'         : None,
        'exch3_energy'         : None,
        'exch4_energy'         : None,
        'tot_energy'           : None,
        'mx'                   : None,
        'my'                   : None,
        'mz'                   : None,
        'typical_energy_joule' : None,
        'typical_energy_kvd'   : None,
        'failed'               : None,
        'restart'              : None
    }

    joule_mode = False
    avg_mag_mode = False
    typical_energy_mode = False
    restart_counter = -1         # This counts the restart blocks
    restart_info = []            # This holds the restart information

    for line in str_stdout:

        match_delta_f = regex_delta_f.match(line)
        if match_delta_f:
            restart_counter += 1

            restart_info.append({
                'delta_f' : float(match_delta_f.group(1)),
                'status'  : 'SUCCESS'
            })
            continue

        match_failed_to_converge = regex_failed_to_converge.match(line)
        if match_failed_to_converge:
            restart_counter += 1

            restart_info.append({
                'delta_f' : None,
                'status'  : 'FAILED'
            })
            continue

        match_joule_mode = regex_joule_mode.match(line)
        if match_joule_mode:
            joule_mode = True
            avg_mag_mode = False
            typical_energy_mode = False
            continue

        match_avg_mag_mode = regex_avg_mag_mode.match(line)
        if match_avg_mag_mode:
            joule_mode = False
            avg_mag_mode = True
            typical_energy_mode = False
            continue

        match_typical_energy = regex_typical_energy_mode.match(line)
        if match_typical_energy:
            joule_mode = False
            avg_mag_mode = False
            typical_energy_mode = True
            continue

        match_anis_energy = regex_anis_energy.match(line)
        if match_anis_energy and joule_mode:
            result['anis_energy'] = float(match_anis_energy.group(1))
            continue

        match_ext_energy = regex_ext_energy.match(line)
        if match_ext_energy and joule_mode:
            result['ext_energy'] = float(match_ext_energy.group(1))
            continue

        match_demag_energy = regex_demag_energy.match(line)
        if match_demag_energy and joule_mode:
            result['demag_energy'] = float(match_demag_energy.group(1))
            continue

        match_exch1_energy = regex_exch1_energy.match(line)
        if match_exch1_energy and joule_mode:
            result['exch1_energy'] = float(match_exch1_energy.group(1))
            continue

        match_exch2_energy = regex_exch2_energy.match(line)
        if match_exch2_energy and joule_mode:
            result['exch2_energy'] = float(match_exch2_energy.group(1))
            continue

        match_exch3_energy = regex_exch3_energy.match(line)
        if match_exch3_energy and joule_mode:
            result['exch3_energy'] = float(match_exch3_energy.group(1))
            continue

        match_exch4_energy = regex_exch4_energy.match(line)
        if match_exch4_energy and joule_mode:
            result['exch4_energy'] = float(match_exch4_energy.group(1))
            continue

        match_tot_energy = regex_tot_energy.match(line)
        if match_tot_energy and joule_mode:
            result['tot_energy'] = float(match_tot_energy.group(1))
            continue

        match_mx = regex_mx.match(line)
        if match_mx and avg_mag_mode:
            result['mx'] = float(match_mx.group(1))
            continue

        match_my = regex_my.match(line)
        if match_my and avg_mag_mode:
            result['my'] = float(match_my.group(1))
            continue

        match_mz = regex_mz.match(line)
        if match_mz and avg_mag_mode:
            result['mz'] = float(match_mz.group(1))
            continue

        match_typical_energy_joule = regex_typical_energy_joule.match(line)
        if match_typical_energy_joule and typical_energy_mode:
            result['typical_energy_joule'] = float(
                match_typical_energy_joule.group(1)
            )
            continue

        match_typical_energy_kvd = regex_typical_energy_kvd.match(line)
        if match_typical_energy_kvd and typical_energy_mode:
            result['typical_energy_kvd'] = float(
                match_typical_energy_kvd.group(1)
            )
            continue

    # Check if there is a failure in restart_info
    result['failed'] = False
    for r in restart_info:
        if r['status'] == 'FAILED':
            result['failed'] = True

    # Add restart_info to result
    result['restart'] = restart_info

    return result
