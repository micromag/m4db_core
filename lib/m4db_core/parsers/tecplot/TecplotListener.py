from antlr4.tree.Tree import TerminalNode

import m4db_core.parsers.gen.tecplot.TecplotListener

class TecplotListener(m4db_core.parsers.gen.tecplot.TecplotListener.TecplotListener):

    def enterTitle(self, ctx):
        print("title")
        for child in ctx.children:
            if isinstance(child, TerminalNode):
                if child.getText() not in ['TITLE', '=']:
                    print("\t'{}'".format(child.getText()))

    def enterVariables(self, ctx):
        print("variables")
        for child in ctx.children:
            if isinstance(child, TerminalNode):
                if child.getText() not in ['VARIABLES', ',', '=']:
                    print("\t'{}'".format(child.getText()))

