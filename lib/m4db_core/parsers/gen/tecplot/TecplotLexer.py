# Generated from Tecplot.g4 by ANTLR 4.8
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys



def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2\24")
        buf.write("\u0097\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7")
        buf.write("\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r")
        buf.write("\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\4\23")
        buf.write("\t\23\3\2\3\2\3\3\3\3\3\3\3\3\3\3\3\3\3\4\3\4\3\4\3\4")
        buf.write("\3\4\3\4\3\4\3\4\3\4\3\4\3\5\3\5\3\5\3\5\3\5\3\6\3\6\3")
        buf.write("\7\3\7\3\b\3\b\3\t\3\t\3\n\3\n\3\n\3\13\3\13\3\13\3\13")
        buf.write("\3\13\3\13\3\13\3\13\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3")
        buf.write("\f\3\f\3\f\3\f\3\r\3\r\6\r`\n\r\r\r\16\ra\3\r\3\r\3\16")
        buf.write("\3\16\3\16\3\17\3\17\3\17\3\17\3\17\5\17n\n\17\7\17p\n")
        buf.write("\17\f\17\16\17s\13\17\3\20\3\20\3\21\6\21x\n\21\r\21\16")
        buf.write("\21y\3\21\3\21\3\22\6\22\177\n\22\r\22\16\22\u0080\3\23")
        buf.write("\5\23\u0084\n\23\3\23\6\23\u0087\n\23\r\23\16\23\u0088")
        buf.write("\3\23\3\23\6\23\u008d\n\23\r\23\16\23\u008e\3\23\3\23")
        buf.write("\3\23\6\23\u0094\n\23\r\23\16\23\u0095\2\2\24\3\3\5\4")
        buf.write("\7\5\t\6\13\7\r\b\17\t\21\n\23\13\25\f\27\r\31\16\33\17")
        buf.write("\35\20\37\21!\22#\23%\24\3\2\7\4\2C\\c|\6\2\f\f\17\17")
        buf.write("$$^^\5\2\13\f\17\17\"\"\3\2\62;\4\2--//\2\u00a0\2\3\3")
        buf.write("\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2")
        buf.write("\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2\2")
        buf.write("\2\25\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2\2\2\2")
        buf.write("\35\3\2\2\2\2\37\3\2\2\2\2!\3\2\2\2\2#\3\2\2\2\2%\3\2")
        buf.write("\2\2\3\'\3\2\2\2\5)\3\2\2\2\7/\3\2\2\2\t9\3\2\2\2\13>")
        buf.write("\3\2\2\2\r@\3\2\2\2\17B\3\2\2\2\21D\3\2\2\2\23F\3\2\2")
        buf.write("\2\25I\3\2\2\2\27Q\3\2\2\2\31]\3\2\2\2\33e\3\2\2\2\35")
        buf.write("h\3\2\2\2\37t\3\2\2\2!w\3\2\2\2#~\3\2\2\2%\u0083\3\2\2")
        buf.write("\2\'(\7.\2\2(\4\3\2\2\2)*\7V\2\2*+\7K\2\2+,\7V\2\2,-\7")
        buf.write("N\2\2-.\7G\2\2.\6\3\2\2\2/\60\7X\2\2\60\61\7C\2\2\61\62")
        buf.write("\7T\2\2\62\63\7K\2\2\63\64\7C\2\2\64\65\7D\2\2\65\66\7")
        buf.write("N\2\2\66\67\7G\2\2\678\7U\2\28\b\3\2\2\29:\7\\\2\2:;\7")
        buf.write("Q\2\2;<\7P\2\2<=\7G\2\2=\n\3\2\2\2>?\7V\2\2?\f\3\2\2\2")
        buf.write("@A\7P\2\2A\16\3\2\2\2BC\7G\2\2C\20\3\2\2\2DE\7H\2\2E\22")
        buf.write("\3\2\2\2FG\7G\2\2GH\7V\2\2H\24\3\2\2\2IJ\7H\2\2JK\7G\2")
        buf.write("\2KL\7R\2\2LM\7Q\2\2MN\7K\2\2NO\7P\2\2OP\7V\2\2P\26\3")
        buf.write("\2\2\2QR\7V\2\2RS\7G\2\2ST\7V\2\2TU\7T\2\2UV\7C\2\2VW")
        buf.write("\7J\2\2WX\7G\2\2XY\7F\2\2YZ\7T\2\2Z[\7Q\2\2[\\\7P\2\2")
        buf.write("\\\30\3\2\2\2]_\7$\2\2^`\t\2\2\2_^\3\2\2\2`a\3\2\2\2a")
        buf.write("_\3\2\2\2ab\3\2\2\2bc\3\2\2\2cd\7$\2\2d\32\3\2\2\2ef\5")
        buf.write("\35\17\2fg\7$\2\2g\34\3\2\2\2hq\7$\2\2ip\n\3\2\2jm\7^")
        buf.write("\2\2kn\13\2\2\2ln\7\2\2\3mk\3\2\2\2ml\3\2\2\2np\3\2\2")
        buf.write("\2oi\3\2\2\2oj\3\2\2\2ps\3\2\2\2qo\3\2\2\2qr\3\2\2\2r")
        buf.write("\36\3\2\2\2sq\3\2\2\2tu\7?\2\2u \3\2\2\2vx\t\4\2\2wv\3")
        buf.write("\2\2\2xy\3\2\2\2yw\3\2\2\2yz\3\2\2\2z{\3\2\2\2{|\b\21")
        buf.write("\2\2|\"\3\2\2\2}\177\t\5\2\2~}\3\2\2\2\177\u0080\3\2\2")
        buf.write("\2\u0080~\3\2\2\2\u0080\u0081\3\2\2\2\u0081$\3\2\2\2\u0082")
        buf.write("\u0084\7/\2\2\u0083\u0082\3\2\2\2\u0083\u0084\3\2\2\2")
        buf.write("\u0084\u0086\3\2\2\2\u0085\u0087\t\5\2\2\u0086\u0085\3")
        buf.write("\2\2\2\u0087\u0088\3\2\2\2\u0088\u0086\3\2\2\2\u0088\u0089")
        buf.write("\3\2\2\2\u0089\u008a\3\2\2\2\u008a\u008c\7\60\2\2\u008b")
        buf.write("\u008d\t\5\2\2\u008c\u008b\3\2\2\2\u008d\u008e\3\2\2\2")
        buf.write("\u008e\u008c\3\2\2\2\u008e\u008f\3\2\2\2\u008f\u0090\3")
        buf.write("\2\2\2\u0090\u0091\7G\2\2\u0091\u0093\t\6\2\2\u0092\u0094")
        buf.write("\t\5\2\2\u0093\u0092\3\2\2\2\u0094\u0095\3\2\2\2\u0095")
        buf.write("\u0093\3\2\2\2\u0095\u0096\3\2\2\2\u0096&\3\2\2\2\r\2")
        buf.write("amoqy\u0080\u0083\u0088\u008e\u0095\3\b\2\2")
        return buf.getvalue()


class TecplotLexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    T__0 = 1
    KW_TITLE = 2
    KW_VARIABLES = 3
    KW_ZONE = 4
    SYMB_ZONE_T = 5
    SYMB_ZONE_N = 6
    SYMB_ZONE_E = 7
    SYMB_ZONE_F = 8
    SYMB_ZONE_ET = 9
    SYMB_F_FEPOINT = 10
    SYMB_ET_TETRAHEDRON = 11
    VARIABLE = 12
    STRING_LITERAL = 13
    UNTERMINATED_STRING_LITERAL = 14
    ASSIGN = 15
    WS = 16
    INT = 17
    FLOAT = 18

    channelNames = [ u"DEFAULT_TOKEN_CHANNEL", u"HIDDEN" ]

    modeNames = [ "DEFAULT_MODE" ]

    literalNames = [ "<INVALID>",
            "','", "'TITLE'", "'VARIABLES'", "'ZONE'", "'T'", "'N'", "'E'", 
            "'F'", "'ET'", "'FEPOINT'", "'TETRAHEDRON'", "'='" ]

    symbolicNames = [ "<INVALID>",
            "KW_TITLE", "KW_VARIABLES", "KW_ZONE", "SYMB_ZONE_T", "SYMB_ZONE_N", 
            "SYMB_ZONE_E", "SYMB_ZONE_F", "SYMB_ZONE_ET", "SYMB_F_FEPOINT", 
            "SYMB_ET_TETRAHEDRON", "VARIABLE", "STRING_LITERAL", "UNTERMINATED_STRING_LITERAL", 
            "ASSIGN", "WS", "INT", "FLOAT" ]

    ruleNames = [ "T__0", "KW_TITLE", "KW_VARIABLES", "KW_ZONE", "SYMB_ZONE_T", 
                  "SYMB_ZONE_N", "SYMB_ZONE_E", "SYMB_ZONE_F", "SYMB_ZONE_ET", 
                  "SYMB_F_FEPOINT", "SYMB_ET_TETRAHEDRON", "VARIABLE", "STRING_LITERAL", 
                  "UNTERMINATED_STRING_LITERAL", "ASSIGN", "WS", "INT", 
                  "FLOAT" ]

    grammarFileName = "Tecplot.g4"

    def __init__(self, input=None, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.8")
        self._interp = LexerATNSimulator(self, self.atn, self.decisionsToDFA, PredictionContextCache())
        self._actions = None
        self._predicates = None


