# Generated from Tecplot.g4 by ANTLR 4.8
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .TecplotParser import TecplotParser
else:
    from TecplotParser import TecplotParser

# This class defines a complete listener for a parse tree produced by TecplotParser.
class TecplotListener(ParseTreeListener):

    # Enter a parse tree produced by TecplotParser#start.
    def enterStart(self, ctx:TecplotParser.StartContext):
        pass

    # Exit a parse tree produced by TecplotParser#start.
    def exitStart(self, ctx:TecplotParser.StartContext):
        pass


    # Enter a parse tree produced by TecplotParser#title.
    def enterTitle(self, ctx:TecplotParser.TitleContext):
        pass

    # Exit a parse tree produced by TecplotParser#title.
    def exitTitle(self, ctx:TecplotParser.TitleContext):
        pass


    # Enter a parse tree produced by TecplotParser#variables.
    def enterVariables(self, ctx:TecplotParser.VariablesContext):
        pass

    # Exit a parse tree produced by TecplotParser#variables.
    def exitVariables(self, ctx:TecplotParser.VariablesContext):
        pass


    # Enter a parse tree produced by TecplotParser#zone.
    def enterZone(self, ctx:TecplotParser.ZoneContext):
        pass

    # Exit a parse tree produced by TecplotParser#zone.
    def exitZone(self, ctx:TecplotParser.ZoneContext):
        pass


    # Enter a parse tree produced by TecplotParser#zone_title.
    def enterZone_title(self, ctx:TecplotParser.Zone_titleContext):
        pass

    # Exit a parse tree produced by TecplotParser#zone_title.
    def exitZone_title(self, ctx:TecplotParser.Zone_titleContext):
        pass


    # Enter a parse tree produced by TecplotParser#zone_n.
    def enterZone_n(self, ctx:TecplotParser.Zone_nContext):
        pass

    # Exit a parse tree produced by TecplotParser#zone_n.
    def exitZone_n(self, ctx:TecplotParser.Zone_nContext):
        pass


    # Enter a parse tree produced by TecplotParser#zone_e.
    def enterZone_e(self, ctx:TecplotParser.Zone_eContext):
        pass

    # Exit a parse tree produced by TecplotParser#zone_e.
    def exitZone_e(self, ctx:TecplotParser.Zone_eContext):
        pass


    # Enter a parse tree produced by TecplotParser#zone_f.
    def enterZone_f(self, ctx:TecplotParser.Zone_fContext):
        pass

    # Exit a parse tree produced by TecplotParser#zone_f.
    def exitZone_f(self, ctx:TecplotParser.Zone_fContext):
        pass


    # Enter a parse tree produced by TecplotParser#zone_f_type.
    def enterZone_f_type(self, ctx:TecplotParser.Zone_f_typeContext):
        pass

    # Exit a parse tree produced by TecplotParser#zone_f_type.
    def exitZone_f_type(self, ctx:TecplotParser.Zone_f_typeContext):
        pass


    # Enter a parse tree produced by TecplotParser#zone_et.
    def enterZone_et(self, ctx:TecplotParser.Zone_etContext):
        pass

    # Exit a parse tree produced by TecplotParser#zone_et.
    def exitZone_et(self, ctx:TecplotParser.Zone_etContext):
        pass


    # Enter a parse tree produced by TecplotParser#zone_et_type.
    def enterZone_et_type(self, ctx:TecplotParser.Zone_et_typeContext):
        pass

    # Exit a parse tree produced by TecplotParser#zone_et_type.
    def exitZone_et_type(self, ctx:TecplotParser.Zone_et_typeContext):
        pass


    # Enter a parse tree produced by TecplotParser#float_list.
    def enterFloat_list(self, ctx:TecplotParser.Float_listContext):
        pass

    # Exit a parse tree produced by TecplotParser#float_list.
    def exitFloat_list(self, ctx:TecplotParser.Float_listContext):
        pass



del TecplotParser