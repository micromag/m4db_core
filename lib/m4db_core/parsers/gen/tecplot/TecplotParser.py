# Generated from Tecplot.g4 by ANTLR 4.8
# encoding: utf-8
from antlr4 import *
from io import StringIO
import sys
if sys.version_info[1] > 5:
	from typing import TextIO
else:
	from typing.io import TextIO


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\24")
        buf.write("]\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b")
        buf.write("\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r\3\2\3\2")
        buf.write("\3\2\3\2\3\3\3\3\3\3\3\3\3\4\3\4\3\4\3\4\3\4\7\4(\n\4")
        buf.write("\f\4\16\4+\13\4\3\5\3\5\3\5\5\5\60\n\5\3\5\3\5\5\5\64")
        buf.write("\n\5\3\5\3\5\5\58\n\5\3\5\3\5\5\5<\n\5\3\5\3\5\3\6\3\6")
        buf.write("\3\6\3\6\3\7\3\7\3\7\3\7\3\b\3\b\3\b\3\b\3\t\3\t\3\t\3")
        buf.write("\t\3\n\3\n\3\13\3\13\3\13\3\13\3\f\3\f\3\r\6\rY\n\r\r")
        buf.write("\r\16\rZ\3\r\2\2\16\2\4\6\b\n\f\16\20\22\24\26\30\2\2")
        buf.write("\2V\2\32\3\2\2\2\4\36\3\2\2\2\6\"\3\2\2\2\b,\3\2\2\2\n")
        buf.write("?\3\2\2\2\fC\3\2\2\2\16G\3\2\2\2\20K\3\2\2\2\22O\3\2\2")
        buf.write("\2\24Q\3\2\2\2\26U\3\2\2\2\30X\3\2\2\2\32\33\5\4\3\2\33")
        buf.write("\34\5\6\4\2\34\35\5\b\5\2\35\3\3\2\2\2\36\37\7\4\2\2\37")
        buf.write(" \7\21\2\2 !\7\17\2\2!\5\3\2\2\2\"#\7\5\2\2#$\7\21\2\2")
        buf.write("$)\7\16\2\2%&\7\3\2\2&(\7\16\2\2\'%\3\2\2\2(+\3\2\2\2")
        buf.write(")\'\3\2\2\2)*\3\2\2\2*\7\3\2\2\2+)\3\2\2\2,-\7\6\2\2-")
        buf.write("/\5\n\6\2.\60\7\3\2\2/.\3\2\2\2/\60\3\2\2\2\60\61\3\2")
        buf.write("\2\2\61\63\5\f\7\2\62\64\7\3\2\2\63\62\3\2\2\2\63\64\3")
        buf.write("\2\2\2\64\65\3\2\2\2\65\67\5\16\b\2\668\7\3\2\2\67\66")
        buf.write("\3\2\2\2\678\3\2\2\289\3\2\2\29;\5\20\t\2:<\7\3\2\2;:")
        buf.write("\3\2\2\2;<\3\2\2\2<=\3\2\2\2=>\5\24\13\2>\t\3\2\2\2?@")
        buf.write("\7\7\2\2@A\7\21\2\2AB\7\17\2\2B\13\3\2\2\2CD\7\b\2\2D")
        buf.write("E\7\21\2\2EF\7\23\2\2F\r\3\2\2\2GH\7\t\2\2HI\7\21\2\2")
        buf.write("IJ\7\23\2\2J\17\3\2\2\2KL\7\n\2\2LM\7\21\2\2MN\5\22\n")
        buf.write("\2N\21\3\2\2\2OP\7\f\2\2P\23\3\2\2\2QR\7\13\2\2RS\7\21")
        buf.write("\2\2ST\5\26\f\2T\25\3\2\2\2UV\7\r\2\2V\27\3\2\2\2WY\7")
        buf.write("\24\2\2XW\3\2\2\2YZ\3\2\2\2ZX\3\2\2\2Z[\3\2\2\2[\31\3")
        buf.write("\2\2\2\b)/\63\67;Z")
        return buf.getvalue()


class TecplotParser ( Parser ):

    grammarFileName = "Tecplot.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "','", "'TITLE'", "'VARIABLES'", "'ZONE'", 
                     "'T'", "'N'", "'E'", "'F'", "'ET'", "'FEPOINT'", "'TETRAHEDRON'", 
                     "<INVALID>", "<INVALID>", "<INVALID>", "'='" ]

    symbolicNames = [ "<INVALID>", "<INVALID>", "KW_TITLE", "KW_VARIABLES", 
                      "KW_ZONE", "SYMB_ZONE_T", "SYMB_ZONE_N", "SYMB_ZONE_E", 
                      "SYMB_ZONE_F", "SYMB_ZONE_ET", "SYMB_F_FEPOINT", "SYMB_ET_TETRAHEDRON", 
                      "VARIABLE", "STRING_LITERAL", "UNTERMINATED_STRING_LITERAL", 
                      "ASSIGN", "WS", "INT", "FLOAT" ]

    RULE_start = 0
    RULE_title = 1
    RULE_variables = 2
    RULE_zone = 3
    RULE_zone_title = 4
    RULE_zone_n = 5
    RULE_zone_e = 6
    RULE_zone_f = 7
    RULE_zone_f_type = 8
    RULE_zone_et = 9
    RULE_zone_et_type = 10
    RULE_float_list = 11

    ruleNames =  [ "start", "title", "variables", "zone", "zone_title", 
                   "zone_n", "zone_e", "zone_f", "zone_f_type", "zone_et", 
                   "zone_et_type", "float_list" ]

    EOF = Token.EOF
    T__0=1
    KW_TITLE=2
    KW_VARIABLES=3
    KW_ZONE=4
    SYMB_ZONE_T=5
    SYMB_ZONE_N=6
    SYMB_ZONE_E=7
    SYMB_ZONE_F=8
    SYMB_ZONE_ET=9
    SYMB_F_FEPOINT=10
    SYMB_ET_TETRAHEDRON=11
    VARIABLE=12
    STRING_LITERAL=13
    UNTERMINATED_STRING_LITERAL=14
    ASSIGN=15
    WS=16
    INT=17
    FLOAT=18

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.8")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None




    class StartContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def title(self):
            return self.getTypedRuleContext(TecplotParser.TitleContext,0)


        def variables(self):
            return self.getTypedRuleContext(TecplotParser.VariablesContext,0)


        def zone(self):
            return self.getTypedRuleContext(TecplotParser.ZoneContext,0)


        def getRuleIndex(self):
            return TecplotParser.RULE_start

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterStart" ):
                listener.enterStart(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitStart" ):
                listener.exitStart(self)




    def start(self):

        localctx = TecplotParser.StartContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_start)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 24
            self.title()
            self.state = 25
            self.variables()
            self.state = 26
            self.zone()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class TitleContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def KW_TITLE(self):
            return self.getToken(TecplotParser.KW_TITLE, 0)

        def ASSIGN(self):
            return self.getToken(TecplotParser.ASSIGN, 0)

        def STRING_LITERAL(self):
            return self.getToken(TecplotParser.STRING_LITERAL, 0)

        def getRuleIndex(self):
            return TecplotParser.RULE_title

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTitle" ):
                listener.enterTitle(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTitle" ):
                listener.exitTitle(self)




    def title(self):

        localctx = TecplotParser.TitleContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_title)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 28
            self.match(TecplotParser.KW_TITLE)
            self.state = 29
            self.match(TecplotParser.ASSIGN)
            self.state = 30
            self.match(TecplotParser.STRING_LITERAL)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class VariablesContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def KW_VARIABLES(self):
            return self.getToken(TecplotParser.KW_VARIABLES, 0)

        def ASSIGN(self):
            return self.getToken(TecplotParser.ASSIGN, 0)

        def VARIABLE(self, i:int=None):
            if i is None:
                return self.getTokens(TecplotParser.VARIABLE)
            else:
                return self.getToken(TecplotParser.VARIABLE, i)

        def getRuleIndex(self):
            return TecplotParser.RULE_variables

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterVariables" ):
                listener.enterVariables(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitVariables" ):
                listener.exitVariables(self)




    def variables(self):

        localctx = TecplotParser.VariablesContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_variables)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 32
            self.match(TecplotParser.KW_VARIABLES)
            self.state = 33
            self.match(TecplotParser.ASSIGN)
            self.state = 34
            self.match(TecplotParser.VARIABLE)
            self.state = 39
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==TecplotParser.T__0:
                self.state = 35
                self.match(TecplotParser.T__0)
                self.state = 36
                self.match(TecplotParser.VARIABLE)
                self.state = 41
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ZoneContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def KW_ZONE(self):
            return self.getToken(TecplotParser.KW_ZONE, 0)

        def zone_title(self):
            return self.getTypedRuleContext(TecplotParser.Zone_titleContext,0)


        def zone_n(self):
            return self.getTypedRuleContext(TecplotParser.Zone_nContext,0)


        def zone_e(self):
            return self.getTypedRuleContext(TecplotParser.Zone_eContext,0)


        def zone_f(self):
            return self.getTypedRuleContext(TecplotParser.Zone_fContext,0)


        def zone_et(self):
            return self.getTypedRuleContext(TecplotParser.Zone_etContext,0)


        def getRuleIndex(self):
            return TecplotParser.RULE_zone

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterZone" ):
                listener.enterZone(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitZone" ):
                listener.exitZone(self)




    def zone(self):

        localctx = TecplotParser.ZoneContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_zone)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 42
            self.match(TecplotParser.KW_ZONE)
            self.state = 43
            self.zone_title()
            self.state = 45
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==TecplotParser.T__0:
                self.state = 44
                self.match(TecplotParser.T__0)


            self.state = 47
            self.zone_n()
            self.state = 49
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==TecplotParser.T__0:
                self.state = 48
                self.match(TecplotParser.T__0)


            self.state = 51
            self.zone_e()
            self.state = 53
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==TecplotParser.T__0:
                self.state = 52
                self.match(TecplotParser.T__0)


            self.state = 55
            self.zone_f()
            self.state = 57
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==TecplotParser.T__0:
                self.state = 56
                self.match(TecplotParser.T__0)


            self.state = 59
            self.zone_et()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Zone_titleContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def SYMB_ZONE_T(self):
            return self.getToken(TecplotParser.SYMB_ZONE_T, 0)

        def ASSIGN(self):
            return self.getToken(TecplotParser.ASSIGN, 0)

        def STRING_LITERAL(self):
            return self.getToken(TecplotParser.STRING_LITERAL, 0)

        def getRuleIndex(self):
            return TecplotParser.RULE_zone_title

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterZone_title" ):
                listener.enterZone_title(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitZone_title" ):
                listener.exitZone_title(self)




    def zone_title(self):

        localctx = TecplotParser.Zone_titleContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_zone_title)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 61
            self.match(TecplotParser.SYMB_ZONE_T)
            self.state = 62
            self.match(TecplotParser.ASSIGN)
            self.state = 63
            self.match(TecplotParser.STRING_LITERAL)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Zone_nContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def SYMB_ZONE_N(self):
            return self.getToken(TecplotParser.SYMB_ZONE_N, 0)

        def ASSIGN(self):
            return self.getToken(TecplotParser.ASSIGN, 0)

        def INT(self):
            return self.getToken(TecplotParser.INT, 0)

        def getRuleIndex(self):
            return TecplotParser.RULE_zone_n

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterZone_n" ):
                listener.enterZone_n(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitZone_n" ):
                listener.exitZone_n(self)




    def zone_n(self):

        localctx = TecplotParser.Zone_nContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_zone_n)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 65
            self.match(TecplotParser.SYMB_ZONE_N)
            self.state = 66
            self.match(TecplotParser.ASSIGN)
            self.state = 67
            self.match(TecplotParser.INT)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Zone_eContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def SYMB_ZONE_E(self):
            return self.getToken(TecplotParser.SYMB_ZONE_E, 0)

        def ASSIGN(self):
            return self.getToken(TecplotParser.ASSIGN, 0)

        def INT(self):
            return self.getToken(TecplotParser.INT, 0)

        def getRuleIndex(self):
            return TecplotParser.RULE_zone_e

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterZone_e" ):
                listener.enterZone_e(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitZone_e" ):
                listener.exitZone_e(self)




    def zone_e(self):

        localctx = TecplotParser.Zone_eContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_zone_e)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 69
            self.match(TecplotParser.SYMB_ZONE_E)
            self.state = 70
            self.match(TecplotParser.ASSIGN)
            self.state = 71
            self.match(TecplotParser.INT)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Zone_fContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def SYMB_ZONE_F(self):
            return self.getToken(TecplotParser.SYMB_ZONE_F, 0)

        def ASSIGN(self):
            return self.getToken(TecplotParser.ASSIGN, 0)

        def zone_f_type(self):
            return self.getTypedRuleContext(TecplotParser.Zone_f_typeContext,0)


        def getRuleIndex(self):
            return TecplotParser.RULE_zone_f

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterZone_f" ):
                listener.enterZone_f(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitZone_f" ):
                listener.exitZone_f(self)




    def zone_f(self):

        localctx = TecplotParser.Zone_fContext(self, self._ctx, self.state)
        self.enterRule(localctx, 14, self.RULE_zone_f)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 73
            self.match(TecplotParser.SYMB_ZONE_F)
            self.state = 74
            self.match(TecplotParser.ASSIGN)
            self.state = 75
            self.zone_f_type()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Zone_f_typeContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def SYMB_F_FEPOINT(self):
            return self.getToken(TecplotParser.SYMB_F_FEPOINT, 0)

        def getRuleIndex(self):
            return TecplotParser.RULE_zone_f_type

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterZone_f_type" ):
                listener.enterZone_f_type(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitZone_f_type" ):
                listener.exitZone_f_type(self)




    def zone_f_type(self):

        localctx = TecplotParser.Zone_f_typeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 16, self.RULE_zone_f_type)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 77
            self.match(TecplotParser.SYMB_F_FEPOINT)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Zone_etContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def SYMB_ZONE_ET(self):
            return self.getToken(TecplotParser.SYMB_ZONE_ET, 0)

        def ASSIGN(self):
            return self.getToken(TecplotParser.ASSIGN, 0)

        def zone_et_type(self):
            return self.getTypedRuleContext(TecplotParser.Zone_et_typeContext,0)


        def getRuleIndex(self):
            return TecplotParser.RULE_zone_et

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterZone_et" ):
                listener.enterZone_et(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitZone_et" ):
                listener.exitZone_et(self)




    def zone_et(self):

        localctx = TecplotParser.Zone_etContext(self, self._ctx, self.state)
        self.enterRule(localctx, 18, self.RULE_zone_et)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 79
            self.match(TecplotParser.SYMB_ZONE_ET)
            self.state = 80
            self.match(TecplotParser.ASSIGN)
            self.state = 81
            self.zone_et_type()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Zone_et_typeContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def SYMB_ET_TETRAHEDRON(self):
            return self.getToken(TecplotParser.SYMB_ET_TETRAHEDRON, 0)

        def getRuleIndex(self):
            return TecplotParser.RULE_zone_et_type

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterZone_et_type" ):
                listener.enterZone_et_type(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitZone_et_type" ):
                listener.exitZone_et_type(self)




    def zone_et_type(self):

        localctx = TecplotParser.Zone_et_typeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 20, self.RULE_zone_et_type)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 83
            self.match(TecplotParser.SYMB_ET_TETRAHEDRON)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Float_listContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def FLOAT(self, i:int=None):
            if i is None:
                return self.getTokens(TecplotParser.FLOAT)
            else:
                return self.getToken(TecplotParser.FLOAT, i)

        def getRuleIndex(self):
            return TecplotParser.RULE_float_list

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterFloat_list" ):
                listener.enterFloat_list(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitFloat_list" ):
                listener.exitFloat_list(self)




    def float_list(self):

        localctx = TecplotParser.Float_listContext(self, self._ctx, self.state)
        self.enterRule(localctx, 22, self.RULE_float_list)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 86 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 85
                self.match(TecplotParser.FLOAT)
                self.state = 88 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not (_la==TecplotParser.FLOAT):
                    break

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx





