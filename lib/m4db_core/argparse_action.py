r"""
This module contains a collection of useful actions that may be used in
command line argument parsers.
"""

import re

from argparse import Action

import m4db_core.util

from m4db_core.global_config import get_global_variables


class SizeAction(Action):
    r'''
    An action that will parse lists of sizes.
    '''
    def __call__(self, parser, namespace, values, option_string=None):
        # The first way to input sizes: a comma separated list of integers
        regex_size_comma_list = re.compile(r'^\d+(,\d+)*$')
        match_size_comma_list = regex_size_comma_list.match(values)
        if match_size_comma_list:
            namespace.size = [int(s.strip()) for s in values.split(',')]
            return

        # The second way to input sizes: a start, end and step
        regex_size_start_end_step = re.compile(r'^(\d+):(\d+):(\d+)$')
        match_size_start_end_step = regex_size_start_end_step.match(values)
        if match_size_start_end_step:
            start_val = int(match_size_start_end_step.group(1))
            end_val = int(match_size_start_end_step.group(2))
            step_val = int(match_size_start_end_step.group(3))
            namespace.size = range(start_val, end_val, step_val)
            return

        parser.error(
            'Unknown format for size, the acceptable formats are:\n{}'.format(
                SizeAction.format()
            )
        )

    @staticmethod
    def format():
        return "<int>[,<int> ...] or <int>:<int>:<int>"


class SizeUnitAction(Action):
    r"""
    An action that will parse a size unit. Using this action
    requires a SizeAction to precede it, upon running it will
    look for 'size' in the namespace and will add 'micron_size'
    to the namespace (i.e. it will convert sizes in 'size' to micron
    by interpreting the unit as whatever)
    """
    def __call__(self, parser, namespace, values, option_string=None):
        gcfg = get_global_variables()

        # Make sure that we already have size
        if namespace.size is None:
            raise ValueError('No sizes given before size unit')

        size_unit_symbols = gcfg.lst_size_units

        if values not in size_unit_symbols:
            raise ValueError("Unrecognized size unit: '{}'".format(values))

        namespace.size_unit = values

        # Add size to micron conversion
        micron_size = []
        for size in namespace.size:
            micron_size.append(m4db_core.util.convert_to_micron(size, namespace.size_unit))
        namespace.micron_size = micron_size


class TemperatureAction(Action):
    r"""
    A class to parse lists of temperatures.
    """
    def __call__(self, parser, namespace, values, option_string=None):
        # The first way to input sizes: a comma separated list of integers
        regex_temperature_comma_list = re.compile(r'^(-?[0-9]+)(,-?[0-9]+)*$')
        match_temperature_comma_list = regex_temperature_comma_list.match(values)
        if match_temperature_comma_list:
            namespace.temperature = [int(s.strip()) for s in values.split(',')]
            return

        # The second way to input sizes: a start, end and step
        regex_temperature_start_end_step = re.compile(r'^(-?[0-9]+):(-?[0-9]+):([0-9]+)$')
        match_temperature_start_end_step = regex_temperature_start_end_step.match(values)
        if match_temperature_start_end_step:
            start_val = int(match_temperature_start_end_step.group(1))
            end_val = int(match_temperature_start_end_step.group(2))
            step_val = int(match_temperature_start_end_step.group(3))
            namespace.temperature = range(start_val, end_val, step_val)
            return

        parser.error(
            'Unknown format for temperature'
        )


class TemperatureUnitAction(Action):
    def __call__(self, parser, namespace, values, option_string=None):
        # Make sure that we already have size
        if namespace.temperature is None:
            raise ValueError('No temperatures given before temperature unit')

        if values not in ['C', 'K', 'F']:
            raise ValueError('Values must be C - centigrade, K - kelvin or F - farenheit')

        namespace.temperature_unit = values

        celsius_temperature = []
        for temperature in namespace.temperature:
            celsius_temperature.append(
                m4db_core.util.convert_to_celsius(
                    temperature, namespace.temperature_unit
                )
            )
        namespace.celsius_temperature = celsius_temperature


class MaterialAction(Action):
    def __call__(self, parser, namespace, values, option_string=None):
        regex_material_comma_list = \
            re.compile(r'^[a-zA-Z][a-zA-Z0-9_]*(,[a-zA-Z][a-zA-Z0-9_]*)*$')

        idx_mat = r'\[([0-9]+)\]([a-zA-Z]([a-zA-Z0-9_]*))'

        regex_multi_material_comma_list = re.compile(
            r'^{imat:}(,{imat:})*$'.format(imat=idx_mat)
        )

        namespace.materials = []

        # Plain comma separated list of materials
        match_material_comma_list = regex_material_comma_list.match(values)
        if match_material_comma_list:
            counter = 0
            for mat_name in [m.strip() for m in values.split(',')]:
                namespace.materials.append(
                    {
                        'index' : counter,
                        'name'  : mat_name
                    }
                )
                counter = counter + 1
            return

        # Comma separated list of materials along with user defined indices
        match_multi_material_comma_list = regex_multi_material_comma_list.match(values)
        if match_multi_material_comma_list:
            # Regex to extract index and material name
            regex_index_material = re.compile(r'^{imat:}$'.format(imat=idx_mat))

            # Split the multi materials on comma
            multi_mats = [s.strip() for s in values.split(',')]

            for imat in multi_mats:
                match_index_material = regex_index_material.match(imat)
                if match_index_material:
                    namespace.materials.append(
                        {
                            'index': int(match_index_material.group(1)),
                            'name' : match_index_material.group(2)
                        }
                    )
                else:
                    # Sanity check, should never happen
                    raise AssertionError("Could not match already matched submatch")
            return

        parser.error('Unknown format for material')


class GeometryAction(Action):
    def __call__(self, parser, namespace, values, option_string=None):
        regex_geometry_comma_list = re.compile(r'^[a-zA-Z][a-zA-Z0-9_]*(,[a-zA-Z][a-zA-Z0-9_]*)*$')
        match_geometry_comma_list = regex_geometry_comma_list.match(values)
        if match_geometry_comma_list:
            namespace.geometry = [g.strip() for g in values.split(',')]
            return

        parser.error('Unknown format for geometry')


class InitialFieldAction(Action):
    def __call__(self, parser, namespace, values, option_string=None):
        gcfg = get_global_variables()

        regex_random = re.compile('random')
        regex_uniform_pair = re.compile(
            r'\[({float:}),({float:})\]'.format(
                float=gcfg.rstr_float
            )
        )
        regex_uniform_triple = re.compile(
            r'\[({float:}),({float:}),({float})\]'.format(
                float=gcfg.rstr_float
            )
        )
        regex_uuid = re.compile(gcfg.rstr_uuid)

        match_random = regex_random.match(values)
        if match_random:
            namespace.initial_field = values
            return

        match_uniform_pair = regex_uniform_pair.match(values)
        if match_uniform_pair:
            namespace.initial_field = values
            return

        match_uniform_triple = regex_uniform_triple.match(values)
        if match_uniform_triple:
            namespace.initial_field = values
            return

        match_uuid = regex_uuid.match(values)
        if match_uuid:
            namespace.initial_field = values
            return

        # Report error here
        parser.error(
            'Unknown format for initial field'
        )


class ExternalFieldAction(Action):
    def __call__(self, parser, namespace, values, option_string=None):
        gcfg = get_global_variables()

        regex_none = re.compile('none')
        regex_uniform_dir_magnitude = re.compile(
            r'\[({float:}),({float:})\]:({float:})(T|mT|uT|nT|pT|fT|aT)'.format(
                float=gcfg.rstr_float
            )
        )
        regex_uniform_triple = re.compile(
            r'\[({float:}),({float:}),({float:})\]:({float:})(T|mT|uT|nT|pT|fT|aT)'.format(
                float=gcfg.rstr_float
            )
        )

        match_none = regex_none.match(values)
        if match_none:
            namespace.external_field = values
            return

        match_uniform_dir_magnitude = regex_uniform_dir_magnitude.match(values)
        if match_uniform_dir_magnitude:
            namespace.external_field = values
            return

        match_uniform_triple = regex_uniform_triple.match(values)
        if match_uniform_triple:
            namespace.external_field = values
            return

        # Report error here
        parser.error(
            'Unknown format for external field'
        )
