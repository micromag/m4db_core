r"""
A set of routines that will update (change) m4db geometry information.
"""
def add_geometry(*params):
    r"""
    Adds a new geometry to the database.
    Args:
        *params: a set of key-value pairs
            name              - the geometry name (string, required)
            size              - the geometry size (Decimal, required)
            size_unit         - the geometry size_unit (string, required)
            element_size      - the geometry element size (Decimal, optional)
            element_size_unit - the geometry element size unit (string, optional)
            description       - the geometry description (string, optional)
    Returns:

    """