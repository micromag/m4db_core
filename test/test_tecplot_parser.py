import unittest

from antlr4 import *
from m4db_core.parsers.gen.tecplot.TecplotLexer import TecplotLexer
from m4db_core.parsers.gen.tecplot.TecplotParser import TecplotParser

from m4db_core.parsers.tecplot.TecplotListener import TecplotListener

class MyTestCase(unittest.TestCase):

    def test_basic_parser(self):
        input_stream = FileStream("../test_files/tecplot/magnetization_old.tec")
        lexer = TecplotLexer(input_stream)
        stream = CommonTokenStream(lexer)
        parser = TecplotParser(stream)
        tree = parser.start()
        listener = TecplotListener()
        walker = ParseTreeWalker()
        walker.walk(listener, tree)


if __name__ == '__main__':
    unittest.main()

