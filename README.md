Description
-----------

These are library files used by m4db utility scripts and the m4db
web service.

Authors:
    L. Nagy, W. Williams

Dependencies
------------

    * sklearn


